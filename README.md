# Ads Mediation

## Contents

* [Install](#install)
* [Setup Ads](#setup-ads)
    * [Open Ads](#open-ads)
    * [Splash Ads](#splash-ads)
    * [Banner Ads](#banner-ads)
    * [Native Ads](#native-ads)
    * [Interstitial Ads](#interstitial-ads)
    * [Reward Ads](#reward-ads)
    * [Reward Interstitial Ads](#reward-interstitial-ads)
* [Setup Data Ads](#setup-data-ads)
    * [Remote config on Firebase (Ads)](#remote-config-on-firebase-ads)
    * [Setting in code (Ads)](#setting-in-code-ads)
    * [Other (Ads)](#other-ads)
* [ID Ads Test](#id-ads-test)
* [Setup Push Update](#setup-push-update)
    * [Remote config on Firebase (Update)](#remote-config-on-firebase-update)
    * [Setting in code (Update)](#setting-in-code-update)
* [Setup Push Rate](#setup-push-rate)
* [In App Purchase](#in-app-purchase)
    * [Features](#features)
    * [ProxPurchase](#proxpurchase)
* [Setup Survey](#setup-survey)
    * [Remote config on Firebase (Survey)](#remote-config-on-firebase-survey)
    * [Setting in code (Survey)](#setting-in-code-survey)
* [Build](#build)

## Install

### build.gradle(Project)

```
allprojects {
	repositories {
        ...
        maven { url 'https://jitpack.io' }
        maven { url "https://android-sdk.is.com" }
        maven { url "https://artifact.bytedance.com/repository/pangle" }
        maven { url 'https://artifacts.applovin.com/android' }
        maven { url "https://dl-maven-android.mintegral.com/repository/mbridge_android_sdk_oversea" }
        maven {
            allowInsecureProtocol = true
            url "$artifactory_contextUrl"
        }
    }
}
```

### gradle.properties

```
android.useAndroidX=true
android.enableJetifier=true

artifactory_contextUrl=http://139.162.53.34:8081/repository/maven-releases/
```

### build.gradle(Module:app)

```
dependencies {
	implementation "prox-lib:prox-utils-multiple-mediation:2.7.1-af"
}
```

### AndroidManifest.xml

```
<?xml version="1.0" encoding="utf-8"?>
<manifest … >
    <application
        android:networkSecurityConfig="@xml/network_security_config"
        … >
        ⋮

        <meta-data
            android:name="applovin.sdk.key"
            android:value="YOUR_SDK_KEY_HERE"/>

        <meta-data
            android:name="com.google.android.gms.ads.APPLICATION_ID"
            android:value="YOUR_APP_ID_HERE" />

        <meta-data
            android:name="com.google.android.gms.ads.AD_MANAGER_APP"
            android:value="true"/>

        <meta-data
            android:name="com.google.android.gms.ads.flag.OPTIMIZE_INITIALIZATION"
            android:value="true"/>

        <meta-data
            android:name="com.google.android.gms.ads.flag.OPTIMIZE_AD_LOADING"
            android:value="true"/>
    </application>
</manifest>
```

res/xml/network_security_config.xml

```
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>

    <!-- For AdColony and Smaato, this permits all cleartext traffic: -->
    <base-config cleartextTrafficPermitted="true">
        <trust-anchors>
            <certificates src="system" />
        </trust-anchors>
    </base-config>
    <!-- End AdColony / Smaato section -->

    <domain-config cleartextTrafficPermitted="true">

        <!-- For Meta Audience Network, this permits cleartext traffic to localhost: -->
        <domain includeSubdomains="true">127.0.0.1</domain>
        <!-- End Meta Audience Network section -->

    </domain-config>
</network-security-config>
```

## Setup Ads

### Open Ads

Your application

```
class App : ProxApplication() {
    override fun onCreate() {
        //Đặt key trùng với key trên remote config 
        AdsUtils.setKeyRemoteConfig("config_ads_remote")
        super.onCreate()

        //Không show AppOpenAds ở SplashActivity
        AdsUtils.registerDisableOpenAdsAt(SplashActivity::class.java)

        //Add Subscription and Product ID
        PurchaseUtils.addSubscriptionId(listOf("subscription_id"))
        PurchaseUtils.addOneTimeProductId(listOf("one_time_product_id"))
        PurchaseUtils.addConsumableId(listOf("consumable_id"))

        //Add Subscription and Product ID remove ads
        PurchaseUtils.setListRemoveAdsId(listOf("premium_subscription"))

        //Tạo Style Native Custom
        AdsUtils.addStyleNative(100, R.layout.custom_ads_native_big_1)
    }
    
    //Đặt key AF
    override fun getAppsFlyerKey(): String {
        return "123456789"
    }
    
    //Bật Roi360
    override fun getRoi360(): Boolean {
        return true
    }
    
    //Khai báo icon app
    override fun getAppIconId(): Int = R.mipmap.ic_launcher
}
```

Disable/Enable open ads

```
AdsUtils.disableOpenAds()
AdsUtils.enableOpenAds()
```

Lắng nghe callback Open Ads

```
AdsUtils.setLoadOpenAdsCallback(object : LoadAdsCallback() {})
AdsUtils.setShowOpenAdsCallback(object : ShowAdsCallback() {})
AdsUtils.setClickOpenAdsCallback(object : ClickAdsCallback() {})
```

### Splash Ads

Load Splash Ads và chuyển màn khi load xong

```
val splashAds = AdsUtils.loadSplashAds(
    this, //Activity
    this  //LifecycleOwner
) {
    if (it == InterstitialAds.Status.LOADED || it == InterstitialAds.Status.ERROR) {
        //Trường hợp load ads thành công hoặc thất bại
        nextScreen()
    }
}

//Trường hợp ko load ads do tắt hoặc đã purchase
if (splashAds == null) {
    nextScreen()
}

```

Show Splash Ads và chuyển màn khi close ads.
Nếu đã load ads trước thì sẽ show. Nếu chưa load thì sẽ tiến hành load rồi show

```
AdsUtils.showSplashAds(
    this, //Activity
    object : ShowAdsCallback() {
        override fun onAdClosed() {
            super.onAdClosed()
            nextScreen()
        }

        override fun onShowFailed(message: String?) {
            super.onShowFailed(message)
            nextScreen()
        }
    },
    object : ClickAdsCallback() {
        override fun onAdClicked(adId: String) {}
    }
)
```

### Banner Ads

```
class MainActivity : AppCompatActivity() {
    private var banner: BannerAds<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        banner = AdsUtils.loadBannerAds(
            this, //Activity
            binding.container,  //Framelayout chứa ads
            "id_banner_screen_1", //id_show_ads trên Remote Config
            object : LoadAdsCallback() {
                override fun onLoadSuccess() {
                    super.onLoadSuccess()
                    banner?.showAds(binding.container)
                }

                override fun onLoadFailed(message: String?) {
                    super.onLoadFailed(message)
                }
            }
        )
        banner?.setClickAdsCallback(object : ClickAdsCallback() {})
    }
    
    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    //onDestroyView() nếu là Fragment
    override fun onDestroy() {
        banner?.destroyAds()
        super.onDestroy()
    }
}
```

### Native Ads

Link native ads styles:
https://www.figma.com/file/Frps9ygu4BIKWyUuBdOlud/Styles?node-id=0%3A1&t=fz6TguDG0IGgzVvu-1

Load and Show Native Ads:

```
class MainActivity : AppCompatActivity() {
    private var native: NativeAds<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        native = ProxAdsCache.instance.loadNativeAds(
            this, //Activity
            binding.nativeContainer,  //Framelayout chứa ads
            "id_native_screen_1", //id_show_ads trên Remote Config
            object : LoadAdsCallback() {
                override fun onLoadSuccess() {
                    super.onLoadSuccess()
                    native?.showAds(binding.nativeContainer)
                }

                override fun onLoadFailed(message: String?) {
                    super.onLoadFailed(message)
                }
            }
        )
        native?.setClickAdsCallback(object : ClickAdsCallback() {})
    }

    //onDestroyView() nếu là Fragment
    override fun onDestroy() {
        native?.destroyAds()
        super.onDestroy()
    }
}
```

Load and Show Native Ads in RecyclerView:

```
class CustomRecyclerAdapter(val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    
    private var native: NativeAds<*>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NATIVE_ADS) {
            val binding = ItemRcvNativeAdsBinding.inflate(LayoutInflater.from(activity), parent, false)
            NativeAdsViewHolder(binding)
        } else {
            val binding = ItemRcvImageBinding.inflate(LayoutInflater.from(activity), parent, false)
            ImageViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NativeAdsViewHolder -> {
                holder.binding.container.removeAllViews()
                if (native == null) {
                    native = AdsUtils.loadNativeAds(
                        activity, //Activity
                        null,
                        "id_native_recyclerview", //id_show_ads trên Remote Config
                        object : LoadAdsCallback() {}
                    }
                }
                native?.showAds(holder.binding.container)
            }
        }
    }
}
```

### Interstitial Ads

Load Ads

```
AdsUtils.loadInterstitialAds(
    this, //Activity
    "id_inter_screen_1" //id_show_ads trên Remote Config
)
AdsUtils.loadInterstitialAds(
    this, //Activity
    "id_inter_screen_1", //id_show_ads trên Remote Config
    object : LoadAdsCallback() {}
)
AdsUtils.loadInterstitialAds(
    this, //Activity
    "id_inter_screen_1", //id_show_ads trên Remote Config
    this, //LifecycleOwner
){ status ->
    when (status) {
        InterstitialAds.Status.NONE -> //Ads đang không làm gì
        InterstitialAds.Status.LOADING -> //Ads đang load
        InterstitialAds.Status.LOADED -> //Ads đã load xong
        InterstitialAds.Status.ERROR -> //Ads load fail
    }
}
```

Show Ads

```
AdsUtils.showInterstitialAds(
    this, //Activity
    "id_inter_screen_1", //id_show_ads trên Remote Config
    object : ShowAdsCallback() {
        override fun onShowFailed(message: String?) {
            super.onShowFailed(message)
            nextScreen()
        }

        override fun onAdClosed() {
            super.onAdClosed()
            nextScreen()
        }
    },
    object : ClickAdsCallback() {
        override fun onAdClicked(adId: String) {}
    }
)
```

### Reward Ads

Load Ads

```
AdsUtils.loadRewardAds(
    this, //Activity
    "id_reward_screen_1" //id_show_ads trên Remote Config
)
AdsUtils.loadRewardAds(
    this, //Activity
    "id_reward_screen_1", //id_show_ads trên Remote Config
    object : LoadAdsCallback() {}
)
AdsUtils.loadRewardAds(
    this, //Activity
    "id_reward_screen_1", //id_show_ads trên Remote Config
    this, //LifecycleOwner
){ status ->
    when (status) {
        InterstitialAds.Status.NONE -> //Ads đang không làm gì
        InterstitialAds.Status.LOADING -> //Ads đang load
        InterstitialAds.Status.LOADED -> //Ads đã load xong
        InterstitialAds.Status.ERROR -> //Ads load fail
    }
}
```

Show Ads

```
AdsUtils.showRewardAds(
    this, //Activity
    "id_reward_screen_1", //id_show_ads trên Remote Config
    object : ShowAdsCallback() {
        override fun onShowFailed(message: String?) {
            super.onShowFailed(message)
            nextScreen()
        }

        override fun onAdClosed() {
            super.onAdClosed()
            nextScreen()
        }
    },
    object : ClickAdsCallback() {
        override fun onAdClicked(adId: String) {}
    }
)
```

### Reward Interstitial Ads

Load Ads

```
AdsUtils.loadRewardInterstitialAds(
    this, //Activity
    "id_reward_interstitial_screen_1" //id_show_ads trên Remote Config
)
AdsUtils.loadRewardInterstitialAds(
    this, //Activity
    "id_reward_interstitial_screen_1", //id_show_ads trên Remote Config
    object : LoadAdsCallback() {}
)
AdsUtils.loadRewardInterstitialAds(
    this, //Activity
    "id_reward_interstitial_screen_1", //id_show_ads trên Remote Config
    this, //LifecycleOwner
){ status ->
    when (status) {
        InterstitialAds.Status.NONE -> //Ads đang không làm gì
        InterstitialAds.Status.LOADING -> //Ads đang load
        InterstitialAds.Status.LOADED -> //Ads đã load xong
        InterstitialAds.Status.ERROR -> //Ads load fail
    }
}
```

Show Ads

```
AdsUtils.showRewardInterstitialAds(
    this, //Activity
    "id_reward_screen_1", //id_show_ads trên Remote Config
    5000L, //Nếu đặt time <= 0 thì show ads luôn, time > 0 thì hiện dialog countdown
    object : ShowAdsCallback() {
        override fun onShowFailed(message: String?) {
            super.onShowFailed(message)
            nextScreen()
        }

        override fun onAdClosed() {
            super.onAdClosed()
            nextScreen()
        }
    },
    object : ClickAdsCallback() {
        override fun onAdClicked(adId: String) {}
    }
)
```

## Setup Data Ads

### Remote config on Firebase (Ads)

Parameter name (key): config_ads_remote

Data type: String

Mẫu value:

```
{
  "status": true,
  "keyaf": null,
  "roi360": null,
  "ads_type": "admob",
  "add_applovin_in_admob": true,
  "add_admob_in_max": true,
  "app_id_pangle": "8025677",
  "app_id_mintegral": "144002",
  "app_key_mintegral": "7c22942b749fe6a6e361b675e96b3ee9",
  "app_open": {
    "status": true,
    "id_ads": {
      "id_admob": "ca-app-pub-3617606523175567/6383394534",
      "id_max": "10ec360815d329e7"
    }
  },
  "splash": {
    "status": true,
    "id_inter_ads": {
      "id_admob": "ca-app-pub-3940256099942544/1033173712",
      "id_max": "06f63a710a71f236"
    },
    "id_app_open_ads": {
      "id_admob": "ca-app-pub-3617606523175567/6383394534",
      "id_max": "10ec360815d329e7"
    },
    "type": "inter",
    "timeout": 10000,
    "max_retry_attempt": 0,
    "dialog_loading": true
  },
  "interstitials": {
    "count_click": 5,
    "delay_count_click": 1,
    "timeout_click": 30000,
    "auto_reload": true,
    "max_retry_attempt": 1,
    "dialog_loading": true,
    "values": [
      {
        "status": true,
        "description": "Interstitial Screen 1 (Home)",
        "ads_type": null,
        "id_ads": {
          "id_admob": "ca-app-pub-3617606523175567/4706478648",
          "id_max": "3e3ad71f1d80cfee",
          "id_pangle": null,
          "id_mintegral": {
            "id_placement": null,
            "id_ad": null
          }
        },
        "id_show_ads": "id_inter_home",
        "count_click": null,
        "delay_count_click": null,
        "timeout_click": null,
        "auto_reload": null,
        "max_retry_attempt": null,
        "dialog_loading": null
      }
    ]
  },
  "rewards": {
    "auto_reload": true,
    "max_retry_attempt": 1,
    "dialog_loading": true,
    "values": [
      {
        "status": true,
        "description": "Reward Screen 2 (Home)",
        "id_ads": {
          "id_admob": "ca-app-pub-3617606523175567/6650115700",
          "id_max": "423fa83d7611326a"
        },
        "id_show_ads": "id_reward_home",
        "auto_reload": false,
        "max_retry_attempt": 1,
        "dialog_loading": null
      }
    ]
  },
  "reward_interstitials": {
    "auto_reload": true,
    "max_retry_attempt": 1,
    "dialog_loading": true,
    "values": [
      {
        "status": true,
        "description": "Reward Interstitial Screen 2 (Home)",
        "id_ads": {
          "id_admob": "ca-app-pub-3940256099942544/5354046379",
          "id_max": "423fa83d7611326a"
        },
        "id_show_ads": "id_reward_interstitial_home",
        "auto_reload": false,
        "max_retry_attempt": 1,
        "dialog_loading": null
      }
    ]
  },
  "banners": {
    "values": [
      {
        "status": true,
        "description": "Banner Home",
        "id_ads": {
          "id_admob": "ca-app-pub-3617606523175567/6706828162",
          "id_max": "adc33ec0ff4185a4"
        },
        "id_show_ads": "id_banner_home",
        "collapsible_type": "bottom"
      }
    ]
  },
  "natives": {
    "values": [
      {
        "status": true,
        "description": "Native Onboard",
        "ads_type": null,
        "id_ads": {
          "id_admob": "ca-app-pub-3617606523175567/7628155457",
          "id_max": "1da60ade514e94d9",
          "id_pangle": null,
          "id_mintegral": {
            "id_placement": null,
            "id_ad": null
          }
        },
        "id_show_ads": "id_native_onboard",
        "style": 21
      }
    ]
  }
}
```

#### Note:

| Element                                               | Attribute                                                  | Value type | Default value | Description                                                                                                                                                                                                                                                         | Obligatory                                                                                                                                            |
|-------------------------------------------------------|------------------------------------------------------------|------------|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| Bật/Tắt ads                                           | status                                                     | Boolean    | true          | Bật(true) / Tắt(false) tất cả các ads                                                                                                                                                                                                                               | Bắt buộc                                                                                                                                              |
| Key AF                                                | keyaf                                                      | String?    | null          | Key AF                                                                                                                                                                                                                                                              | Không bắt buộc                                                                                                                                        |
| Bật/Tắt Roi360                                        | roi360           z                                         | Boolean?   | false         | Bật(true) / Tắt(false) Roi360                                                                                                                                                                                                                                       | Không bắt buộc                                                                                                                                        |
| Loại ads                                              | ads_type                                                   | String     | admob         | Loại mediation ads. 1 trong 2 giá trị "admob" (Admob mediation) hoặc "max" (MAX mediation)                                                                                                                                                                          | Bắt buộc                                                                                                                                              |
| Bật/Tắt Applovin (Admob mediation)                    | add_applovin_in_admob                                      | Boolean    | false         | Bật(true) / Tắt (false) Adnet Applovin khi sử dụng Admob mediation                                                                                                                                                                                                  | Không bắt buộc <br/>Lưu ý: Chỉ bắt buộc nếu có gọi riêng Ads Applovin. Trước đó hãy đảm bảo là đã khai báo key Applovin trong manifest                |
| Bật/Tắt Admob (Max mediation)                         | add_admob_in_max                                           | Boolean    | false         | Bật(true) / Tắt (false) Adnet Admob khi sử dụng Max mediation                                                                                                                                                                                                       | Không bắt buộc <br/>Lưu ý: Chỉ bắt buộc nếu có gọi riêng Ads Admob. Trước đó hãy đảm bảo là đã khai báo key Admob trong manifest                      |
| App ID Pangle                                         | app_id_pangle                                              | String?    | null          | App ID của Pangle Ads                                                                                                                                                                                                                                               | Không bắt buộc <br/>Lưu ý: Chỉ bắt buộc nếu có gọi riêng Ads Pangle.                                                                                  |
| App ID Mintegral                                      | app_id_mintegral                                           | String?    | null          | App ID của Mintegral Ads                                                                                                                                                                                                                                            | Không bắt buộc <br/>Lưu ý: Chỉ bắt buộc nếu có gọi riêng Ads Mintegral.                                                                               |
| App Key Mintegral                                     | app_key_mintegral                                          | String?    | null          | App Key của Mintegral Ads                                                                                                                                                                                                                                           | Không bắt buộc <br/>Lưu ý: Chỉ bắt buộc nếu có gọi riêng Ads Mintegral.                                                                               |
| (App Open Ad) Bật/Tắt ad                              | app_open.status                                            | Boolean    | true          | Bật(true) / Tắt(false) App Open Ad                                                                                                                                                                                                                                  | Bắt buộc                                                                                                                                              |
| (App Open Ad) Admob Ad ID                             | app_open.id_ads.id_admob                                   | String?    | null          | ID App Open Ad của Admob                                                                                                                                                                                                                                            | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" thì "app_open.id_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty                                     |
| (App Open Ad) MAX Ad ID                               | app_open.id_ads.id_max                                     | String?    | null          | ID App Open Ad của MAX                                                                                                                                                                                                                                              | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" thì "app_open.id_ads.id_max" phải có giá trị hợp lệ ≠ null và empty                                         |
| (Splash Ad) Bật/Tắt ad                                | splash.status                                              | Boolean    | true          | Bật(true) / Tắt(false) Splash Ad                                                                                                                                                                                                                                    | Bắt buộc                                                                                                                                              |
| (Splash Ad) Admob Interstitial Ad ID                  | splash.id_inter_ads.id_admob                               | String?    | null          | ID Interstitial Ad của Admob dùng làm Splash Ad                                                                                                                                                                                                                     | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" và "splash.type": "inter" thì "splash.id_inter_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty       |
| (Splash Ad) MAX Interstitial Ad ID                    | splash.id_inter_ads.id_max                                 | String?    | null          | ID Interstitial Ad của MAX dùng làm Splash Ad                                                                                                                                                                                                                       | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" và "splash.type": "inter" thì "splash.id_inter_ads.id_max" phải có giá trị hợp lệ ≠ null và empty           |
| (Splash Ad) Admob App Open Ad ID                      | splash.id_app_open_ads.id_admob                            | String?    | null          | ID App Open Ad của Admob dùng làm Splash Ad                                                                                                                                                                                                                         | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" và "splash.type": "open" thì "splash.id_app_open_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty     |
| (Splash Ad) MAX App Open Ad ID                        | splash.id_app_open_ads.id_max                              | String?    | null          | ID App Open Ad của MAX dùng làm Splash Ad                                                                                                                                                                                                                           | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" và "splash.type": "open" thì "splash.id_app_open_ads.id_max" phải có giá trị hợp lệ ≠ null và empty         |
| (Splash Ad) Loại ad                                   | splash.type                                                | String     | inter         | Loại Splash Ad. 1 trong 2 giá trị "inter" (Interstitial Ad) hoặc "open" (App Open Ad)                                                                                                                                                                               | Bắt buộc                                                                                                                                              |
| (Splash Ad) Timeout ad                                | splash.timeout                                             | Int        | 10000         | Thời gian chờ để load Splash Ad (milliseconds) (Bắt buộc giá trị ≥ 0)                                                                                                                                                                                               | Bắt buộc                                                                                                                                              |
| (Splash Ad) Max retry attempt                         | splash.max_retry_attempt                                   | Int        | 0             | Số lần load lại Splash Ad sau khi load lần đầu false (Bắt buộc giá trị ≥ 0)                                                                                                                                                                                         | Không bắt buộc                                                                                                                                        |
| (Splash Ad) Bật/Tắt Dialog loading                    | splash.dialog_loading                                      | Boolean    | true          | Bật(true) / Tắt(false) dialog loading trước khi show Splash Ad                                                                                                                                                                                                      | Không bắt buộc                                                                                                                                        |
| (Interstitial Ads) Bật/Tắt tần suất click ads         | interstitials.status_count_click                           | Boolean    | true          | Bật(true) / Tắt(false) tất cả tần suất click dùng chung của tất cả các Interstitial Ads                                                                                                                                                                             | Không bắt buộc<br/>Lưu ý: Nếu "interstitials.status_count_click": false thì các "interstitials.values[...].count_click" phải có giá trị hợp lệ ≠ null |
| (Interstitial Ads) Tần suất click ads                 | interstitials.count_click                                  | Int        | 1             | Tần suất click dùng chung của tất cả các Interstitial Ads (Bắt buộc giá trị ≥ 1)                                                                                                                                                                                    | Bắt buộc                                                                                                                                              |
| (Interstitial Ads) Delay tần suất click ads           | interstitials.delay_count_click                            | Int        | 0             | Số lần delay click dùng chung khi click trước khi thực hiện tần suất click dùng chung của tất cả các Interstitial Ads (Bắt buộc giá trị ≥ 0)                                                                                                                        | Không bắt buộc                                                                                                                                        |
| (Interstitial Ads) Bật/Tắt tần suất timeout click ads | interstitials.status_timeout_click                         | Boolean    | true          | Bật(true) / Tắt(false) tất cả tần suất timeout click dùng chung của tất cả các Interstitial Ads                                                                                                                                                                     | Không bắt buộc                                                                                                                                        |
| (Interstitial Ads) Tần suất timeout click ads         | interstitials.timeout_click                                | Int        | 0             | Tần suất timeout click dùng chung của tất cả các Interstitial Ads (milliseconds) (Bắt buộc giá trị ≥ 0)                                                                                                                                                             | Bắt buộc                                                                                                                                              |
| (Interstitial Ads) Bật/Tắt auto reload ads            | interstitials.auto_reload                                  | Boolean    | true          | Bật(true) / Tắt(false) load lại của tất cả các Interstitial Ads sau khi show thành công hoặc thất bại                                                                                                                                                               | Không bắt buộc                                                                                                                                        |
| (Interstitial Ads) Max retry attempt                  | interstitials.max_retry_attempt                            | Int        | 1             | Số lần load lại của tất cả các Interstitial Ads sau khi load lần đầu false (Bắt buộc giá trị ≥ 0)                                                                                                                                                                   | Không bắt buộc                                                                                                                                        |
| (Interstitial Ads) Bật/Tắt Dialog loading             | interstitials.dialog_loading                               | Boolean    | true          | Bật(true) / Tắt(false) dialog loading của tất cả các Interstitial Ads trước khi show Interstitial Ad                                                                                                                                                                | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Mô tả ad                            | interstitials.values[...].description                      | String     | empty         | Mô tả vị trí show Interstitial Ad                                                                                                                                                                                                                                   | Bắt buộc                                                                                                                                              |
| (Interstitial Ad) Loại Ad                             | interstitials.values[...].ads_type                         | String?    | null          | Loại Interstitial Ad: 1 trong 5 giá trị "null", "admob", "max", "pangle", "mintegral"                                                                                                                                                                               | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Admob Interstitial Ad ID            | interstitials.values[...].id_ads.id_admob                  | String?    | null          | ID Interstitial Ad của Admob dùng cho vị trí này                                                                                                                                                                                                                    | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" thì "interstitials.values[...].id_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty                    |
| (Interstitial Ad) MAX Interstitial Ad ID              | interstitials.values[...].id_ads.id_max                    | String?    | null          | ID Interstitial Ad của MAX dùng cho vị trí này                                                                                                                                                                                                                      | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" thì "interstitials.values[...].id_ads.id_max" phải có giá trị hợp lệ ≠ null và empty                        |
| (Interstitial Ad) Pangle Interstitial Ad ID           | interstitials.values[...].id_ads.id_pangle                 | String?    | null          | ID Interstitial Ad của Pangle dùng cho vị trí này                                                                                                                                                                                                                   | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Mintegral Interstitial Placement ID | interstitials.values[...].id_ads.id_mintegral.id_placement | String?    | null          | ID Interstitial Placement của Mintegral dùng cho vị trí này                                                                                                                                                                                                         | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Mintegral Interstitial Ad ID        | interstitials.values[...].id_ads.id_mintegral.id_ad        | String?    | null          | ID Interstitial Ad của Mintegral dùng cho vị trí này                                                                                                                                                                                                                | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) ID Show Interstitial Ad             | interstitials.values[...].id_show_ads                      | String?    | null          | ID vị trí sẽ show Interstitial Ad                                                                                                                                                                                                                                   | Bắt buộc<br/>Lưu ý: "interstitials.values[...].id_show_ads" phải có giá trị hợp lệ ≠ null, empty và duy nhất                                          |
| (Interstitial Ad) Tần suất click ad                   | interstitials.values[...].count_click                      | Int?       | null          | Tần suất click Interstitial Ad dùng cho vị trí này (Bắt buộc giá trị là null hoặc ≥ 1). Nếu giá trị ≠ null và ≥ 1, tần suất click Interstitial Ad cho vị trí này sẽ tách riêng khỏi tần suất click dùng chung ("interstitials.count_click")                         | Bắt buộc<br/>Lưu ý: Nếu "interstitials.status_count_click": false thì các "interstitials.values[...].count_click" phải có giá trị hợp lệ ≠ null       |
| (Interstitial Ad) Delay tần suất click ad             | interstitials.values[...].delay_count_click                | Int?       | null          | Số lần delay click dùng cho vị trí này khi click trước khi thực hiện tần suất click (Bắt buộc giá trị là null hoặc ≥ 0). Nếu giá trị ≠ null và ≥ 0, Số lần delay click cho vị trí này sẽ thay thế số lần delay click dùng chung ("interstitials.delay_count_click") | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Tần suất timeout click ad           | interstitials.values[...].timeout_click                    | Int?       | null          | Tần suất timeout click Interstitial Ad dùng cho vị trí này (milliseconds) (Bắt buộc là null hoặc ≥ 0). Nếu giá trị ≠ null và ≥ 0, Tần suất timeout click dùng cho vị trí này sẽ thay thế tần suất timeout click dùng chung ("interstitials.timeout_click")          | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Bật/Tắt auto reload ad              | interstitials.values[...].auto_reload                      | Boolean?   | null          | Bật(true) / Tắt(false) load lại của Interstitial Ad cho vị trí này sau khi show thành công hoặc thất bại                                                                                                                                                            | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Max retry attempt                   | interstitials.values[...].max_retry_attempt                | Int?       | null          | Số lần load lại của Interstitial Ad cho vị trí này sau khi load lần đầu false (Bắt buộc giá trị là null hoặc ≥ 0)                                                                                                                                                   | Không bắt buộc                                                                                                                                        |
| (Interstitial Ad) Bật/Tắt Dialog loading              | interstitials.values[...].dialog_loading                   | Boolean?   | null          | Bật(true) / Tắt(false) load lại Interstitial Ad cho vị trí này sau khi show thành công hoặc thất bại                                                                                                                                                                | Không bắt buộc                                                                                                                                        |
| (Reward Ads) Bật/Tắt auto reload ads                  | rewards.auto_reload                                        | Boolean    | true          | Bật(true) / Tắt(false) load lại của tất cả các Reward Ads sau khi show thành công hoặc thất bại                                                                                                                                                                     | không bắt buộc                                                                                                                                        |
| (Reward Ads) Max retry attempt                        | rewards.max_retry_attempt                                  | Int        | 1             | Số lần load lại của tất cả các Reward Ads sau khi load lần đầu false (Bắt buộc giá trị ≥ 0)                                                                                                                                                                         | không bắt buộc                                                                                                                                        |
| (Reward Ads) Bật/Tắt Dialog loading                   | rewards.dialog_loading                                     | Boolean    | true          | Bật(true) / Tắt(false) dialog loading của tất cả các Reward Ads trước khi show Reward Ad                                                                                                                                                                            | không bắt buộc                                                                                                                                        |
| (Reward Ad) Bật/Tắt ad                                | rewards.values[...].status                                 | Boolean    | true          | Bật(true) / Tắt(false) Reward Ad tại riêng vị trí đó. Chỉ show Reward Ad khi "rewards.values[...].status": true và "rewards.status": true                                                                                                                           | Bắt buộc                                                                                                                                              |
| (Reward Ad) Mô tả ad                                  | rewards.values[...].description                            | String     | empty         | Mô tả vị trí show Reward Ad                                                                                                                                                                                                                                         | Bắt buộc                                                                                                                                              |
| (Reward Ad) Admob Reward Ad ID                        | rewards.values[...].id_ads.id_admob                        | String?    | null          | ID Reward Ad của Admob dùng cho vị trí này                                                                                                                                                                                                                          | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" thì "rewards.values[...].id_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty                          |
| (Reward Ad) MAX Reward Ad ID                          | rewards.values[...].id_ads.id_max                          | String?    | null          | ID Reward Ad của MAX dùng cho vị trí này                                                                                                                                                                                                                            | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" thì "rewards.values[...].id_ads.id_max" phải có giá trị hợp lệ ≠ null và empty                              |
| (Reward Ad) ID Show Reward Ad                         | rewards.values[...].id_show_ads                            | String?    | null          | ID vị trí sẽ show Reward Ad                                                                                                                                                                                                                                         | Bắt buộc<br/>Lưu ý: "rewards.values[...].id_show_ads" phải có giá trị hợp lệ ≠ null, empty và duy nhất                                                |
| (Reward Ad) Bật/Tắt auto reload ad                    | rewards.values[...].auto_reload                            | Boolean?   | null          | Bật(true) / Tắt(false) load lại của Reward Ad cho vị trí này sau khi show thành công hoặc thất bại                                                                                                                                                                  | Không bắt buộc                                                                                                                                        |
| (Reward Ad) Max retry attempt                         | rewards.values[...].max_retry_attempt                      | Int?       | null          | Số lần load lại của Reward Ad cho vị trí này sau khi load lần đầu false (Bắt buộc giá trị là null hoặc ≥ 0)                                                                                                                                                         | Không bắt buộc                                                                                                                                        |
| (Reward Ad) Bật/Tắt Dialog loading                    | rewards.values[...].dialog_loading                         | Boolean?   | null          | Bật(true) / Tắt(false) load lại Reward Ad cho vị trí này sau khi show thành công hoặc thất bại                                                                                                                                                                      | Không bắt buộc                                                                                                                                        |
| (Banner Ad) Bật/Tắt ad                                | banners.values[...].status                                 | Boolean    | true          | Bật(true) / Tắt(false) Banner Ad tại riêng vị trí đó. Chỉ show Banner Ad khi "banners.values[...].status": true và "banners.status": true                                                                                                                           | Bắt buộc                                                                                                                                              |
| (Banner Ad) Mô tả ad                                  | banners.values[...].description                            | String     | empty         | Mô tả vị trí show Banner Ad                                                                                                                                                                                                                                         | Bắt buộc                                                                                                                                              |
| (Banner Ad) Admob Banner Ad ID                        | banners.values[...].id_ads.id_admob                        | String?    | null          | ID Banner Ad của Admob dùng cho vị trí này                                                                                                                                                                                                                          | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" thì "banners.values[...].id_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty                          |
| (Banner Ad) MAX Banner Ad ID                          | banners.values[...].id_ads.id_max                          | String?    | null          | ID Banner Ad của MAX dùng cho vị trí này                                                                                                                                                                                                                            | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" thì "banners.values[...].id_ads.id_max" phải có giá trị hợp lệ ≠ null và empty                              |
| (Banner Ad) ID Show Banner Ad                         | banners.values[...].id_show_ads                            | String?    | null          | ID vị trí sẽ show Banner Ad                                                                                                                                                                                                                                         | Bắt buộc<br/>Lưu ý: "banners.values[...].id_show_ads" phải có giá trị hợp lệ ≠ null, empty và duy nhất                                                |
| (Banner Ad) Collapsible Banner Ad                     | banners.values[...].collapsible_type                       | String?    | null          | Loại Collapsible Banner Ad. 1 trong 3 giá trị "top" (Collapsible Banner Ad từ trên thả xuống), "bottom" (Collapsible Banner Ad từ dưới chui trên), null (Không sử dụng Collapsible Banner Ad)                                                                       | Bắt buộc                                                                                                                                              |
| (Native Ad) Bật/Tắt ad                                | natives.values[...].status                                 | Boolean    | true          | Bật(true) / Tắt(false) Native Ad tại riêng vị trí đó. Chỉ show Native Ad khi "natives.values[...].status": true và "natives.status": true                                                                                                                           | Bắt buộc                                                                                                                                              |
| (Native Ad) Mô tả ad                                  | natives.values[...].description                            | String     | empty         | Mô tả vị trí show Native Ad                                                                                                                                                                                                                                         | Bắt buộc                                                                                                                                              |
| (Native Ad) Loại Ad                                   | natives.values[...].ads_type                               | String?    | null          | Loại Natives Ad: 1 trong 5 giá trị "null", "admob", "max", "pangle", "mintegral"                                                                                                                                                                                    | Không bắt buộc                                                                                                                                        |
| (Native Ad) Admob Native Ad ID                        | natives.values[...].id_ads.id_admob                        | String?    | null          | ID Native Ad của Admob dùng cho vị trí này                                                                                                                                                                                                                          | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "admob" thì "natives.values[...].id_ads.id_admob" phải có giá trị hợp lệ ≠ null và empty                          |
| (Native Ad) MAX Native Ad ID                          | natives.values[...].id_ads.id_max                          | String?    | null          | ID Native Ad của MAX dùng cho vị trí này                                                                                                                                                                                                                            | Bắt buộc<br/>Lưu ý: Nếu "ads_type": "max" thì "natives.values[...].id_ads.id_max" phải có giá trị hợp lệ ≠ null và empty                              |
| (Native Ad) Pangle Native Ad ID                       | natives.values[...].id_ads.id_pangle                       | String?    | null          | ID Native Ad của Pangle dùng cho vị trí này                                                                                                                                                                                                                         | Không bắt buộc                                                                                                                                        |
| (Native Ad) Mintegral Native Placement ID             | natives.values[...].id_ads.id_mintegral.id_placement       | String?    | null          | ID Native Placement của Mintegral dùng cho vị trí này                                                                                                                                                                                                               | Không bắt buộc                                                                                                                                        |
| (Native Ad) Mintegral Native Ad ID                    | natives.values[...].id_ads.id_mintegral.id_ad              | String?    | null          | ID Native Ad của Mintegral dùng cho vị trí này                                                                                                                                                                                                                      | Không bắt buộc                                                                                                                                        |
| (Native Ad) ID Show Native Ad                         | natives.values[...].id_show_ads                            | String?    | null          | ID vị trí sẽ show Native Ad                                                                                                                                                                                                                                         | Bắt buộc<br/>Lưu ý: "natives.values[...].id_show_ads" phải có giá trị hợp lệ ≠ null, empty và duy nhất                                                |
| (Native Ad) Style Native Ad                           | natives.values[...].style                                  | String?    | null          | Kiểu Native Ad                                                                                                                                                                                                                                                      | Bắt buộc<br/>Lưu ý: "natives.values[...].style" phải có giá trị hợp lệ ≠ null                                                                         |

### Setting in code (Ads)

Lấy value từ remote config --> Mã hóa bằng tool ToolAESGCM rồi lưu value đã mã hóa vào
raw/config_ads_local.txt

- Link tải tool mã
  hóa: https://gitlab.com/proxglobal/android/proxlib/-/blob/release/v2.1.2/tool/file%20run/ToolAESGCM-1.2.0.jar
- Yêu cầu tối thiểu sử dụng toolAESGCM: Cài đặt từ JDK 11 trở lên
- Mở toolAESGCM bằng JavaLauncher
- Đối với MACOS từ Big Sur (11.2), cần phải cấp quyền truy cập file. Cách cấp quyền: System Setting
  -> Privacy & Security -> Full Disk Access nhấn vào dấu + để thêm JavaLauncher (đường dẫn:
  /System/Library/CoreServices/JavaLauncher.app)

### Other (Ads)

Link native ads styles:
https://www.figma.com/file/Frps9ygu4BIKWyUuBdOlud/Native-ads-styles?node-id=0%3A1&t=2EWQqqjM0G9Q1I8s-1

If you want to use custom native ads: layout_native_custom.xml

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout>

    <FrameLayout
        android:id="@+id/ad_options_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:minWidth="15dp"
        android:minHeight="15dp"
        />

    <com.google.android.gms.ads.nativead.MediaView
        android:id="@+id/ad_media"
        />

    <ImageView
        android:id="@+id/ad_app_icon"
        />

    <RatingBar
        android:id="@+id/ad_rating"
        />

    <TextView
        android:id="@+id/ad_headline"
        />

    <TextView
        android:id="@+id/ad_advertiser"
        />

    <TextView
        android:id="@+id/ad_body"
        />

    <TextView
        android:id="@+id/ad_store"
        />
        
    <TextView
        android:id="@+id/ad_price"
        />
        
    <Button
        android:id="@+id/ad_call_to_action"
        />

</androidx.constraintlayout.widget.ConstraintLayout>
```

## ID Ads Test

Admob

```
Application ID: ca-app-pub-3940256099942544~3347511713
App Open ID: ca-app-pub-3940256099942544/9257395921
Banner ID: ca-app-pub-3940256099942544/9214589741
Interstitial ID: ca-app-pub-3940256099942544/1033173712
Interstitial Video ID: ca-app-pub-3940256099942544/8691691433
Rewarded ID: ca-app-pub-3940256099942544/5224354917
Native ID: ca-app-pub-3940256099942544/2247696110
Native Video ID: ca-app-pub-3940256099942544/1044960115
```

Pangle

```
App ID: 8025677
Interstitial Vertical ID: 980088188
Interstitial Horizontal ID: 980099798
Rewarded Vertical ID: 980088192
Rewarded Horizontal ID: 980099801
Banner ID: 980099802
Native ID: 980088216
App Open Vertical ID: 890000078
App Open Horizontal ID: 890000079
```

Mintegral

```
App Key: 7c22942b749fe6a6e361b675e96b3ee9
App ID: 144002 
Banner Placement ID: 1010694
Banner Ad ID: 2677210
Native Placement ID: 290656
Native Ad ID: 462377
Rewarded Placement ID: 290651
Rewarded Ad ID: 462372
Interstitial Placement ID: 290653
Interstitial Ad ID: 462374
```

## Setup Push Update

### Remote config on Firebase (Update)

```
Parameter name (key): config_update_version                                 //Default, can be changed
Data type: String
Default value:
{  
    "required": false,                                                                                          //(Required) Turn on/off required update
    "version_code_required": [                                                                                  //(Required) List of versions that must be updated
  	    1  
    ],

    "status": true,                                                                                             //(Option) Turn on/off optional update
    "version_code": 3,                                                                                          //(Option) Version code will update to
    "version_name": "Version 1.0.2",                                                                            //(Option) Version name will update to

    "title": "Update New Version",                                                                              //Title content
    "message": "Updated version with many performance improvements and new features. Update your apps now!",    //Message content
    "new_package": ""                                                                                           //package app needs link to store. If value is empty, will link to current app store
}
```

### Setting in code (Update)

```
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ...
        UpdateUtils.show(
            this, //Activity
            BuildConfig.VERSION_CODE, //Version app.
            R.drawable.ic_app,  //Icon App
            getString(R.string.app_name), //Name App
            BuildConfig.DEBUG
        )
    }
    ...
}
```

## Setup Push Rate

Initialize dialog rate

```
val config = ProxRateConfig()
config.listener = object : RatingDialogListener() {
	override fun onRated() {
        `//TO-DO`
	}

	override fun onSubmitButtonClicked(rate: Int, comment: String) {
        `//TO-DO`
	}

	override fun onLaterButtonClicked() {
        `//TO-DO`
	}

	override fun onChangeStar(rate: Int) {
        `//TO-DO`
	}

	override fun onDone() {
        `//TO-DO`
	}
}
RateUtils.init()
RateUtils.setConfig(config)
```

Show dialog rate

```
RateUtils.showAlways(fm: FragmentManager)                         //Always show
RateUtils.showIfNeed(context: Context, fm: FragmentManager)       //Show only if not rate yet
```

## In App Purchase

### Features

- ProxPurchase với thư viện Google Play Billing 5. Xem thêm về [thư viện  Google Billing]

## ProxPurchase

- Add all your product id in onCreate() of your Application

```
PurchaseUtils.addSubscriptionId(listOf("subscription_id"))
PurchaseUtils.addOneTimeProductId(listOf("one_time_product_id"))
PurchaseUtils.addConsumableId(listOf("consumable_id"))
```

- Get Price

```
PurchaseUtils.getPrice(id) // id can be basePlanId, offerId, or oneTimeProductId

```

- Make a purchase

```
PurchaseUtils.addPurchaseUpdateListener(object : PurchaseUpdateListener{
    override fun onPurchaseFailure(code: Int, errorMsg: String?) {
    }

    override fun onUserCancelBilling() {
    }
    
    override fun onOwnedProduct(p0: String) {
    }

    override fun onPurchaseSuccess(productId: String) {
        //purchase successed
    }
})

PurchaseUtils.buy(activity, id) // id can be basePlanId, offerId, or oneTimeProductId
```

- Kiểm tra purchase

```
PurchaseUtils.setActionPurchase(
    {
        //cập nhật UI hoặc event khi đã Purchase
    },
    {
        //cập nhật UI hoặc event khi chưa Purchase
    }
)
```

- After ProxPurchase's initiation finish, you can get information about products. If you need query
  these informations early, you need add initBillingFinishListner

```
PurchaseUtils.addInitBillingFinishListener {
    if(ProxPurchase.getInstance().checkPurchased()) {
        getGoToMain()
    } else {
        showAds()
    }
}
```

[thư viện Google Billing]: <https://support.google.com/googleplay/android-developer/answer/12154973?hl=vi&ref_topic=345289>

[đây]: <https://www.figma.com/file/cqG2LMeQvsKliLBKBZEmFq/Document_Remote_Sale?node-id=0%3A1>

[Gulp]: <http://gulpjs.com>

[PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>

[PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>

[PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>

[PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>

[PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>

[PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>

## Setup Survey

### Remote config on Firebase (Survey)

```
Parameter name (key): config_survey                                 //Default, can be changed
Data type: String
Default value:
{      
    "style": 1,                                                     //Style Survey
    "status": true,                                                 //Turn on/off survey
    "survey_name": "Survey name", 
    
    "question_1": "Question 1",      
    "hint_answer_1": "Hint answer 1",      
    "option_1_1": "Option 1_1",      
    "option_1_2": "Option 1_2",      
    "option_1_3": "Option 1_3",      
    "option_1_4": "Option 1_4",      
    
    "question_2": "Question 2",      
    "hint_answer_2": "Hint answer 2",      
    "option_2_1": "Option 2_1",      
    "option_2_2": "Option 2_2",      
    "option_2_3": "Option 3_3",      
    "option_2_4": "Option 4_4" 
}
```

### Setting in code (Survey)

```
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ...
        ProxSurveyConfig().showSurveyIfNecessary(activity: AppCompatActivity, isDebug: Boolean)
    }
    ...
}
```

# Build

## gradle.properties

```
projectName=prox-utils
projectType=<type>
currentVersion=<version>
```

## Terminal

```
./gradlew publishReleasePublicationToMavenRepository
```
