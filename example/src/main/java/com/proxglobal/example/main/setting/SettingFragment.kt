package com.proxglobal.example.main.setting

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.ads.pro.base.BannerAds
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.google.ads.pro.type.AdsType
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.BuildConfig
import com.proxglobal.example.databinding.FragmentSettingBinding
import com.proxglobal.example.iap.IapActivity
import com.proxglobal.purchase.PurchaseUtils
import com.proxglobal.rate.RateUtils
import com.proxglobal.survey.SurveyUtils

class SettingFragment : Fragment() {
    private lateinit var binding: FragmentSettingBinding

    private var banner: BannerAds<*>? = null
    private var native: NativeAds<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSettingBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadAds()
        initView()
        addEvent()
    }

    private fun addEvent() {
        binding.btnAdInspectorDebug.setOnClickListener {
            AdsUtils.showAdInspectorDebug(requireContext())
        }

        binding.btnMediationDebug.setOnClickListener {
            AdsUtils.showMaxMediationDebug(requireContext())
        }

        binding.btnShowSurvey.setOnClickListener {
            SurveyUtils.showSurveyIfNecessary(requireActivity(), BuildConfig.DEBUG)
        }

        binding.btnShowRate.setOnClickListener {
            RateUtils.showAlways(childFragmentManager)
        }

        binding.btnScreenIap.setOnClickListener {
            startActivity(Intent(requireContext(), IapActivity::class.java))
        }

        binding.btnRestart.setOnClickListener {
            val restartIntent = requireContext().packageManager.getLaunchIntentForPackage(requireContext().packageName)!!
            restartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(restartIntent)
        }
    }

    private fun initView() {
        if (PurchaseUtils.isRemoveAds()) {
            binding.btnScreenIap.visibility = View.GONE
        } else {
            binding.btnScreenIap.visibility = View.VISIBLE
        }
    }

    private fun loadAds() {
        AdsUtils.observeLoadAds(this) {
            when (AdsUtils.isAdsType()) {
                AdsType.MAX -> {
                    binding.txtMediation.text = "Type mediation: MAX"
                    binding.btnMediationDebug.visibility = View.VISIBLE
                    binding.btnAdInspectorDebug.visibility = View.GONE
                }

                AdsType.ADMOB -> {
                    binding.txtMediation.text = "Type mediation: ADMOB"
                    binding.btnMediationDebug.visibility = View.GONE
                    binding.btnAdInspectorDebug.visibility = View.VISIBLE
                }

                else -> {
                    binding.txtMediation.text = "Type mediation: UNKNOWN"
                    binding.btnMediationDebug.visibility = View.GONE
                    binding.btnAdInspectorDebug.visibility = View.GONE
                }
            }

            native = AdsUtils.loadNativeAds(
                requireActivity(),
                binding.containerNativeAds,
                "id_native_setting",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        native?.showAds(binding.containerNativeAds)
                    }
                }
            )

            banner = AdsUtils.loadBannerAds(
                requireActivity(),
                binding.containerBannerAds,
                "id_banner_setting",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        banner?.showAds(binding.containerBannerAds)
                    }
                }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    override fun onDestroy() {
        banner?.destroyAds()
        native?.destroyAds()
        super.onDestroy()
    }
}