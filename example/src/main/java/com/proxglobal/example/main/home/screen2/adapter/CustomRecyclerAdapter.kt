package com.proxglobal.example.main.home.screen2.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.databinding.ItemAdsRcvBinding
import com.proxglobal.example.databinding.ItemRcvBinding

class CustomRecyclerAdapter(val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var native1: NativeAds<*>? = null
    private var native2: NativeAds<*>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ADS) {
            val binding = ItemAdsRcvBinding.inflate(LayoutInflater.from(activity), parent, false)
            AdsViewHolder(binding)
        } else {
            val binding = ItemRcvBinding.inflate(LayoutInflater.from(activity), parent, false)
            ItemViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AdsViewHolder -> {
                if (position % 8 == 0) {
                    if (native1 == null) {
                        native1 = AdsUtils.loadNativeAds(
                            activity,
                            null,
                            "id_native_screen_2",
                            object : LoadAdsCallback() {}
                        )
                    }
                    native1?.showAds(holder.binding.root)
                } else {
                    if (native2 == null) {
                        native2 = AdsUtils.loadNativeAds(
                            activity,
                            null,
                            "id_native_screen_2",
                            object : LoadAdsCallback() {}
                        )
                    }
                    native2?.showAds(holder.binding.root)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return 1000
    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 4 == 0) ADS else NORMAL
    }

    inner class ItemViewHolder(binding: ItemRcvBinding) :
        RecyclerView.ViewHolder(binding.root) {
        internal val binding: ItemRcvBinding

        init {
            this.binding = binding
        }
    }

    inner class AdsViewHolder(binding: ItemAdsRcvBinding) :
        RecyclerView.ViewHolder(binding.root) {
        internal val binding: ItemAdsRcvBinding

        init {
            this.binding = binding
        }
    }

    companion object {
        private const val NORMAL = 0
        private const val ADS = 1
    }
}