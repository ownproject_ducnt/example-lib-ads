package com.proxglobal.example.main.home.screen2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.ads.pro.base.BannerAds
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.databinding.FragmentScreen2Binding
import com.proxglobal.example.main.home.screen2.adapter.CustomRecyclerAdapter

class Screen2Fragment : Fragment() {
    private lateinit var binding: FragmentScreen2Binding
    private lateinit var customRecyclerAdapter: CustomRecyclerAdapter

    private var banner: BannerAds<*>? = null
    private var native: NativeAds<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentScreen2Binding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadAds()
        initView()
        addEvent()
    }

    private fun addEvent() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initView() {
        customRecyclerAdapter = CustomRecyclerAdapter(requireActivity())
        binding.rcv.apply {
            adapter = customRecyclerAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun loadAds() {
        AdsUtils.observeLoadAds(this) {
            banner = AdsUtils.loadBannerAds(
                requireActivity(),
                binding.containerBannerAds,
                "id_banner_screen_2",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        banner?.showAds(binding.containerBannerAds)
                    }
                }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    override fun onDestroy() {
        banner?.destroyAds()
        native?.destroyAds()
        super.onDestroy()
    }
}