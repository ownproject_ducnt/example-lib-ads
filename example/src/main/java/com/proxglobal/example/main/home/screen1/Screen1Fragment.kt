package com.proxglobal.example.main.home.screen1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.ads.pro.base.BannerAds
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.google.ads.pro.callback.ShowAdsCallback
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.R
import com.proxglobal.example.databinding.FragmentScreen1Binding

class Screen1Fragment : Fragment() {
    private lateinit var binding: FragmentScreen1Binding

    private var banner: BannerAds<*>? = null
    private var native: NativeAds<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentScreen1Binding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadAds()
        addEvent()
    }

    private fun addEvent() {
        binding.apply {
            btnBack.setOnClickListener {
                findNavController().popBackStack()
            }

            btnScreen2.setOnClickListener {
                AdsUtils.showInterstitialAds(requireActivity(), "id_inter_screen_1", object : ShowAdsCallback() {
                    override fun onShowFailed(message: String?) {
                        super.onShowFailed(message)
                        nextScreen()
                    }

                    override fun onAdClosed() {
                        super.onAdClosed()
                        nextScreen()
                    }

                    private fun nextScreen() {
                        findNavController().navigate(R.id.screen2Fragment)
                    }
                })
            }
        }
    }

    private fun loadAds() {
        AdsUtils.observeLoadAds(this) {
            native = AdsUtils.loadNativeAds(
                requireActivity(),
                binding.containerNativeAds,
                "id_native_screen_1",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        native?.showAds(binding.containerNativeAds)
                    }
                }
            )

            banner = AdsUtils.loadBannerAds(
                requireActivity(),
                binding.containerBannerAds,
                "id_banner_screen_1",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        banner?.showAds(binding.containerBannerAds)
                    }
                }
            )

            AdsUtils.loadInterstitialAds(requireActivity(), "id_inter_screen_1")
        }
    }

    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    override fun onDestroy() {
        banner?.destroyAds()
        native?.destroyAds()
        super.onDestroy()
    }
}