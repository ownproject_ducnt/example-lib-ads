package com.proxglobal.example.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.ads.pro.base.BannerAds
import com.google.ads.pro.base.InterstitialAds
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.google.ads.pro.callback.ShowAdsCallback
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.R
import com.proxglobal.example.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    private var banner: BannerAds<*>? = null
    private var native: NativeAds<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadAds()
        addEvent()
    }

    private fun addEvent() {
        binding.apply {
            btnScreen1.setOnClickListener {
                AdsUtils.showInterstitialAds(requireActivity(), "id_inter_home", object : ShowAdsCallback() {
                    override fun onShowFailed(message: String?) {
                        super.onShowFailed(message)
                        nextScreen()
                    }

                    override fun onAdClosed() {
                        super.onAdClosed()
                        nextScreen()
                    }

                    private fun nextScreen() {
                        findNavController().navigate(R.id.screen1Fragment)
                    }
                })
            }

            btnScreen2.setOnClickListener {
                AdsUtils.showRewardAds(requireActivity(), "id_reward_home", object : ShowAdsCallback() {
                    override fun onShowFailed(message: String?) {
                        super.onShowFailed(message)
                        nextScreen()
                    }

                    override fun onAdClosed() {
                        super.onAdClosed()
                        nextScreen()
                    }

                    private fun nextScreen() {
                        findNavController().navigate(R.id.screen2Fragment)
                    }
                })
            }
        }
    }

    private fun loadAds() {
        AdsUtils.observeLoadAds(this) {
            native = AdsUtils.loadNativeAds(
                requireActivity(),
                binding.containerNativeAds,
                "id_native_home",
                object : LoadAdsCallback() {
                    override fun onLoadSuccess() {
                        super.onLoadSuccess()
                        native?.showAds(binding.containerNativeAds)
                    }
                }
            )

            AdsUtils.loadInterstitialAds(requireActivity(), "id_inter_home")
            AdsUtils.removeObserversLoadRewardAds("id_reward_home", this)
            AdsUtils.loadRewardAds(requireActivity(), "id_reward_home", this) { status ->
                when (status) {
                    InterstitialAds.Status.NONE -> binding.btnScreen2.text = "Screen 2 (Reward ads) NONE"
                    InterstitialAds.Status.LOADING -> binding.btnScreen2.text = "Screen 2 (Reward ads) LOADING"
                    InterstitialAds.Status.LOADED -> binding.btnScreen2.text = "Screen 2 (Reward ads) LOADED"
                    InterstitialAds.Status.ERROR -> binding.btnScreen2.text = "Screen 2 (Reward ads) ERROR"
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    override fun onDestroy() {
        banner?.destroyAds()
        native?.destroyAds()
        super.onDestroy()
    }
}