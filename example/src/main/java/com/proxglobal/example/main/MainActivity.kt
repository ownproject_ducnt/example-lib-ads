package com.proxglobal.example.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.google.ads.pro.base.BannerAds
import com.google.ads.pro.base.NativeAds
import com.google.ads.pro.callback.LoadAdsCallback
import com.google.ads.pro.callback.ShowAdsCallback
import com.google.rate.ProxRateConfig
import com.google.rate.RatingDialogListener
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.BuildConfig
import com.proxglobal.example.R
import com.proxglobal.example.databinding.ActivityMainBinding
import com.proxglobal.rate.RateUtils
import com.proxglobal.update.UpdateUtils

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private var banner: BannerAds<*>? = null
    private var native: NativeAds<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRemoteConfigPushUpdate()
        initRate()

        loadAds()
        initView()
        addEvent()
    }

    private fun addEvent() {
        navController.addOnDestinationChangedListener { _: NavController?, navDestination: NavDestination, _: Bundle? ->
            when (navDestination.id) {
                R.id.homeFragment -> {
                    if (banner == null) {
                        banner = AdsUtils.loadBannerAds(this, binding.containerBannerAds, "id_banner_home", object : LoadAdsCallback() {})
                    }
                    banner?.showAds(binding.containerBannerAds)
                    binding.bnv.visibility = View.VISIBLE
                    binding.containerBannerAds.visibility = View.VISIBLE
                }

                R.id.settingFragment -> {
                    banner?.destroyAds()
                    banner = null
                    binding.bnv.visibility = View.VISIBLE
                    binding.containerBannerAds.visibility = View.GONE
                }

                else -> {
                    banner?.destroyAds()
                    banner = null
                    binding.bnv.visibility = View.GONE
                    binding.containerBannerAds.visibility = View.GONE
                }
            }
        }

        binding.bnv.setOnItemSelectedListener {
            if (navController.currentDestination?.id == it.itemId) return@setOnItemSelectedListener true
            when (it.itemId) {
                R.id.settingFragment -> {
                    AdsUtils.showInterstitialAds(this, "id_inter_setting", object : ShowAdsCallback() {
                        override fun onShowFailed(message: String?) {
                            super.onShowFailed(message)
                            nextScreen()
                        }

                        override fun onAdClosed() {
                            super.onAdClosed()
                            nextScreen()
                        }

                        private fun nextScreen() {
                            NavigationUI.onNavDestinationSelected(it, navController)
                        }
                    })
                }

                else -> {
                    NavigationUI.onNavDestinationSelected(it, navController)
                }
            }
            return@setOnItemSelectedListener true
        }
    }

    private fun loadAds() {
        AdsUtils.observeLoadAds(this) {
            if (navController.currentDestination?.id == R.id.homeFragment){
                if (banner == null) {
                    banner = AdsUtils.loadBannerAds(this, binding.containerBannerAds, "id_banner_home", object : LoadAdsCallback() {})
                }
                banner?.showAds(binding.containerBannerAds)
            }
            AdsUtils.loadInterstitialAds(this, "id_inter_setting")
        }
    }

    private fun initView() {
        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.homeFragment, R.id.settingFragment))
        binding.bnv.setupWithNavController(navController)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController)
                || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    override fun onResume() {
        super.onResume()
        banner?.resumeAds()
    }

    override fun onPause() {
        banner?.pauseAds()
        super.onPause()
    }

    override fun onDestroy() {
        banner?.destroyAds()
        banner = null
        native?.destroyAds()
        native = null
        super.onDestroy()
    }

    private fun initRemoteConfigPushUpdate() {
        UpdateUtils.show(
            this,
            BuildConfig.VERSION_CODE,
            R.mipmap.ic_launcher,
            getString(R.string.app_name),
            null
        )
    }

    private fun initRate() {
        val config = ProxRateConfig()
        config.foregroundIcon = AppCompatResources.getDrawable(this, R.mipmap.ic_launcher_round)
        config.isCanceledOnTouchOutside = true
        config.listener = object : RatingDialogListener() {
            override fun onRated() {
                super.onRated()
                Toast.makeText(this@MainActivity, "Rated", Toast.LENGTH_SHORT).show()
            }

            override fun onSubmitButtonClicked(rate: Int, comment: String) {
                Toast.makeText(this@MainActivity, "Submit", Toast.LENGTH_SHORT).show()
            }

            override fun onLaterButtonClicked() {
                Toast.makeText(this@MainActivity, "Later", Toast.LENGTH_SHORT).show()
            }

            override fun onChangeStar(rate: Int) {
                Toast.makeText(this@MainActivity, "Star change", Toast.LENGTH_SHORT).show()
            }

            override fun onDone() {
                Toast.makeText(this@MainActivity, "Done", Toast.LENGTH_SHORT).show()
            }
        }
        RateUtils.init()
        RateUtils.setConfig(config)
    }
}