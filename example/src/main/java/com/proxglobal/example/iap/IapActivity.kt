package com.proxglobal.example.iap

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.ads.pro.purchase.PurchaseUpdateListener
import com.google.ads.pro.purchase.model.Purchase
import com.proxglobal.example.databinding.ActivityIapBinding
import com.proxglobal.purchase.PurchaseUtils

class IapActivity : AppCompatActivity() {

    private lateinit var binding: ActivityIapBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIapBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        addEvent()
    }

    private fun addEvent() {
        PurchaseUtils.addPurchaseUpdateListener(object : PurchaseUpdateListener {
            override fun onPurchaseFailure(code: Int, errorMsg: String?) {

            }

            override fun onUserCancelBilling() {

            }

            override fun onOwnedProduct(p0: String) {

            }

            override fun onPurchaseSuccess(purchases: Purchase) {

            }
        })

        binding.btnPurchaseTest1.setOnClickListener {
            PurchaseUtils.buy(this, "monthly")
        }

        binding.btnPurchaseTest1.setOnClickListener {
            PurchaseUtils.buy(this, "yearly")
        }

        binding.btnPurchaseTest1.setOnClickListener {
            PurchaseUtils.buy(this, "in_app_product_1")
        }
    }

    private fun initView() {
        binding.btnPurchaseTest1.text = PurchaseUtils.getPrice("monthly")
        binding.btnPurchaseTest2.text = PurchaseUtils.getPrice("yearly")
        binding.btnPurchaseTest3.text = PurchaseUtils.getPrice("in_app_product_1")
    }
}