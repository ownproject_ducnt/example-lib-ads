package com.proxglobal.example.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.ads.pro.callback.ShowAdsCallback
import com.orhanobut.hawk.Hawk
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.databinding.ActivitySplashBinding
import com.proxglobal.example.main.MainActivity
import com.proxglobal.example.onboard.OnboardActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AdsUtils.observeLoadAds(this) {
            if (Hawk.get(OnboardActivity.IS_START_ONBOARD, true)) {
                AdsUtils.loadNativeAds(this@SplashActivity, "id_native_onboard", 3, true)
            }

            AdsUtils.showSplashAds(this, object : ShowAdsCallback() {
                override fun onShowFailed(message: String?) {
                    super.onShowFailed(message)
                    nextScreen()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    nextScreen()
                }

                private fun nextScreen() {
                    if (Hawk.get(OnboardActivity.IS_START_ONBOARD, true)) {
                        startActivity(Intent(this@SplashActivity, OnboardActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        })
                    } else {
                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                    }
                    finish()
                }
            })
        }
    }
}