package com.proxglobal.example

import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.orhanobut.hawk.Hawk
import com.proxglobal.ads.AdsUtils
import com.proxglobal.ads.application.ProxApplication
import com.proxglobal.example.splash.SplashActivity
import com.proxglobal.purchase.PurchaseUtils

open class App : ProxApplication() {

    override fun onCreate() {
        MobileAds.setRequestConfiguration(RequestConfiguration.Builder().setTestDeviceIds(listOf("47D9E7406BD5B0230635362E08E48F2A")).build())
        super.onCreate()
        Hawk.init(this).build()

        PurchaseUtils.addSubscriptionAndProduct((listOf("remove_ads")), (listOf("in_app_product_1")), listOf("consumable_product"))
        PurchaseUtils.setListRemoveAdsId(listOf("remove_ads"))

        AdsUtils.addStyleNative(100, R.layout.ads_native_big_custom)
        AdsUtils.registerDisableOpenAdsAt(SplashActivity::class.java)
    }
}

