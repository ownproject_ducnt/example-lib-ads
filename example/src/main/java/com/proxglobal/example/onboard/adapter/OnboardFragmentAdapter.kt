package com.proxglobal.example.onboard.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.proxglobal.example.onboard.OnboardFragment

class OnboardFragmentAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fm, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        return OnboardFragment.newInstance(position)
    }

    override fun getItemCount(): Int {
        return 3
    }
}