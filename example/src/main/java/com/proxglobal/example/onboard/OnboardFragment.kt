package com.proxglobal.example.onboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.proxglobal.example.R

class OnboardFragment : Fragment() {

    companion object {
        const val INDEX = "INDEX"

        fun newInstance(index: Int): OnboardFragment {
            val args = Bundle()
            args.putInt(INDEX, index)
            val fragment = OnboardFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var index = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            index = it.getInt(INDEX)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return when (index) {
            0 -> inflater.inflate(R.layout.fragment_onboard_0, container, false)
            1 -> inflater.inflate(R.layout.fragment_onboard_1, container, false)
            else -> inflater.inflate(R.layout.fragment_onboard_2, container, false)
        }
    }
}