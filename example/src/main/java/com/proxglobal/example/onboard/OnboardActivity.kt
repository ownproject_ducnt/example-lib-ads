package com.proxglobal.example.onboard

import android.content.Intent
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.orhanobut.hawk.Hawk
import com.proxglobal.ads.AdsUtils
import com.proxglobal.example.databinding.ActivityOnboardBinding
import com.proxglobal.example.main.MainActivity
import com.proxglobal.example.onboard.adapter.OnboardFragmentAdapter

class OnboardActivity : AppCompatActivity() {

    companion object {
        const val IS_START_ONBOARD = "IS_START_ONBOARD"
    }

    private lateinit var binding: ActivityOnboardBinding
    private lateinit var onboardFragmentAdapter: OnboardFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        addEvent()
    }

    private fun addEvent() {
        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val currentItem = binding.vp.currentItem
                if (currentItem > 0) {
                    binding.vp.currentItem = currentItem - 1
                } else {
                    finish()
                }
            }
        })

        binding.apply {
            next.setOnClickListener {
                val currentItem = binding.vp.currentItem
                if (currentItem < 2) {
                    binding.vp.currentItem = currentItem + 1
                } else {
                    Hawk.put(IS_START_ONBOARD, false)
                    startActivity(Intent(this@OnboardActivity, MainActivity::class.java))
                    finish()
                }
            }

            vp.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    AdsUtils.showNativeAds(binding.containerNativeAds, "id_native_onboard", position)
                }
            })
        }
    }

    private fun initView() {
        onboardFragmentAdapter = OnboardFragmentAdapter(supportFragmentManager, lifecycle)
        binding.vp.adapter = onboardFragmentAdapter
    }
}